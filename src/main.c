#define _GNU_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>
#include <sys/mman.h>

#define HEAP_SIZE 4096
#define NEW_REGION 5000

bool flag = true;

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static void failMessage() {
    printf("Test failed\n");
    flag = false;
}

static void successMessage() {
    printf("Test passed\n\n");
}

static void malloc_test() {
    printf("Test 1: normal memory allocation\n");
    void* block = _malloc(100);
    struct block_header* block_ = block_get_header(block);
    if (block_ -> capacity.bytes != 100 || block_ -> is_free != false) {
        failMessage();
        return;
    }

    _free(block);
    if (block_ -> is_free == false){
        failMessage();
        return;
    }

    successMessage();
}

static void free_one_block_test() {
    printf("Test 2: Release of one block from several allocated blocks\n");

    void* block1 = _malloc(100);
    struct block_header* block_1 = block_get_header(block1);

    void* block2 = _malloc(200);
    struct block_header* block_2 = block_get_header(block2);

    void* block3 = _malloc(300);
    struct block_header* block_3 = block_get_header(block3);

    _free(block1);

    if (block_1 -> is_free != true || block_2 -> is_free != false || block_3 -> is_free != false){
        failMessage();
        return;
    } else {
        successMessage();
    }

    _free(block2);
    _free(block3);
}

static void free_two_blocks_test() {
    printf("Test 3: Releasing two blocks from multiple selections\n");
    void* block1 = _malloc(10);
    struct block_header* block_1 = block_get_header(block1);
    void* block2 = _malloc(20);
    struct block_header* block_2 = block_get_header(block2);
    void* block3 = _malloc(30);
    struct block_header* block_3 = block_get_header(block3);
    _free(block1);
    _free(block3);
    if (block_1 -> is_free != true || block_2 -> is_free != false || block_3 -> is_free != true){
        failMessage();
        return;
    } else {
        successMessage();
    }

    _free(block2);
}

static void new_region_expands_test(void* heap) {
    printf("Test 4: new memory region extends old memory region\n");

    //размер блока больше, чем размер кучи
    struct block_header* heap_ = (struct block_header*) heap;
    size_t before_heap_size = heap_ -> capacity.bytes;
    void* new_block = _malloc(NEW_REGION);
    struct block_header* new__block = block_get_header(new_block);

    if (new__block -> capacity.bytes != NEW_REGION || (before_heap_size >= heap_ -> capacity.bytes)){
        failMessage();
        return;
    }

    successMessage();
    _free(new_block);
}

static void new_region_not_expands_test() {
    printf("Test 5: a new region is allocated elsewhere\n");

    void* before_allocation = map_pages((const void *)HEAP_START, 10, MAP_FIXED);
    void* after_allocation = _malloc(20);

    if(before_allocation == after_allocation){
        failMessage();
        return;
    }

    successMessage();
    _free(after_allocation);
}

int main() {
    void* heap = heap_init(HEAP_SIZE);

    malloc_test();
    free_one_block_test();
    free_two_blocks_test();
    new_region_expands_test(heap);
    new_region_not_expands_test();

    if (!flag) {
        printf("Not all tests were passed\n");
    } else {
        printf("All tests passed successfully!\n");
    }

    heap_term();
}
